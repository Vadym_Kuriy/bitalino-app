package com.bitalino.bitalino.ui;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bitalino.bitalino.GlideApp;
import com.bitalino.bitalino.R;
import com.bitalino.bitalino.util.SensorDataConverter;

import java.util.ArrayList;

import info.plux.pluxapi.Communication;
import info.plux.pluxapi.bitalino.BITalinoCommunication;
import info.plux.pluxapi.bitalino.BITalinoCommunicationFactory;
import info.plux.pluxapi.bitalino.BITalinoDescription;
import info.plux.pluxapi.bitalino.BITalinoException;
import info.plux.pluxapi.bitalino.BITalinoFrame;
import info.plux.pluxapi.bitalino.BITalinoState;
import info.plux.pluxapi.bitalino.bth.OnBITalinoDataAvailable;

import static info.plux.pluxapi.Constants.ACTION_COMMAND_REPLY;
import static info.plux.pluxapi.Constants.ACTION_DATA_AVAILABLE;
import static info.plux.pluxapi.Constants.ACTION_DEVICE_READY;
import static info.plux.pluxapi.Constants.ACTION_EVENT_AVAILABLE;
import static info.plux.pluxapi.Constants.ACTION_STATE_CHANGED;
import static info.plux.pluxapi.Constants.EXTRA_COMMAND_REPLY;
import static info.plux.pluxapi.Constants.EXTRA_DATA;
import static info.plux.pluxapi.Constants.EXTRA_STATE_CHANGED;
import static info.plux.pluxapi.Constants.IDENTIFIER;
import static info.plux.pluxapi.Constants.States;

/**
 * Created by VADIM on 01.05.2017.
 */

public class DeviceActivity extends Activity implements OnBITalinoDataAvailable, View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();

    public final static String EXTRA_DEVICE = "info.plux.pluxapi.sampleapp.DeviceActivity.EXTRA_DEVICE";
    public final static String FRAME = "info.plux.pluxapi.sampleapp.DeviceActivity.Frame";

    public static final int REQUEST_CODE_IMAGE_PICK = 500;
    private static final int PERMISSION_ACCESS_READ_EXTERNAL_STORAGE = 3;

//    private ArrayList<BluetoothDevice> bluetoothDevices;

    private ArrayList<BITalinoCommunication> bitalino;
    private Handler handler;
    private States currentState = States.DISCONNECTED;

    private boolean isUpdateReceiverRegistered = false;

    /*
     * UI elements
     */

    private ImageView imageView1;
    private ImageView imageView2;

//    private Button connectButton;
//    private Button disconnectButton;
//    private Button startButton;
//    private Button stopButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().hasExtra(EXTRA_DEVICE)) {
//            bluetoothDevices = getIntent().getParcelableArrayListExtra(EXTRA_DEVICE);
        }

        setContentView(R.layout.activity_main);

        initView();
        bitalino = new ArrayList<>();
        setUIElements();

        handler = new Handler(getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Bundle bundle = msg.getData();
                BITalinoFrame frame = bundle.getParcelable(FRAME);

                if (frame != null) { //BITalino
                    processResult(frame);
                }
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_device, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.select_image:
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            PERMISSION_ACCESS_READ_EXTERNAL_STORAGE);
                } else {
                    openGallery();
                }
                break;
            case R.id.connect_button:
                for (BITalinoCommunication biTalinoCommunication : bitalino) {
                    try {
                        int index = bitalino.indexOf(biTalinoCommunication);
                        biTalinoCommunication.connect("20:15:10:26:64:86");//bluetoothDevices.get(index).getAddress());
                    } catch (BITalinoException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.disconnect_button:
                for (BITalinoCommunication biTalinoCommunication : bitalino) {
                    try {
                        biTalinoCommunication.disconnect();
                    } catch (BITalinoException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.start_button:
                for (BITalinoCommunication biTalinoCommunication : bitalino) {
                    try {
                        biTalinoCommunication.start(new int[]{2}, 500);
                    } catch (BITalinoException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(updateReceiver, makeUpdateIntentFilter());
        isUpdateReceiverRegistered = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isUpdateReceiverRegistered) {
            unregisterReceiver(updateReceiver);
            isUpdateReceiverRegistered = false;
        }

        if (bitalino != null) {
            for (BITalinoCommunication bitalinoCommunication : bitalino) {
                bitalinoCommunication.closeReceivers();
                try {
                    bitalinoCommunication.disconnect();
                } catch (BITalinoException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        final Uri uri;
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_IMAGE_PICK:
                    uri = data != null && data.getData() != null ? data.getData() : null;
                    GlideApp
                            .with(this)
                            .load(uri)
                            .centerCrop()
                            .into(imageView1);
                    GlideApp
                            .with(this)
                            .load(uri)
                            .centerCrop()
                            .into(imageView2);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery();
                }
                break;
        }
    }

    /*
     * UI elements
     */
    private void initView() {
        invalidateOptionsMenu();
        imageView1 = (ImageView) findViewById(R.id.image_1);
        imageView2 = (ImageView) findViewById(R.id.image_2);
//        connectButton = (Button) findViewById(R.id.connect_button);
//        disconnectButton = (Button) findViewById(R.id.disconnect_button);
//        startButton = (Button) findViewById(R.id.start_button);
//        stopButton = (Button) findViewById(R.id.stop_button);
    }

    private void setUIElements() {
//        for (BluetoothDevice bluetoothDevice : bluetoothDevices) {
//            final int index = bluetoothDevices.indexOf(bluetoothDevice);
        Communication communication = Communication.BTH;//Communication.getById(bluetoothDevices.get(index).getType());
        Log.d(TAG, "Communication: " + communication.name());
        if (communication.equals(Communication.DUAL)) {
            communication = Communication.BLE;
        }
        bitalino.add(new BITalinoCommunicationFactory().getCommunication(communication, this, this));
//        }
//        connectButton.setOnClickListener(this);
//        disconnectButton.setOnClickListener(this);
//        startButton.setOnClickListener(this);
//        stopButton.setOnClickListener(this);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_IMAGE_PICK);
    }

    /*
     * Local Broadcast
     */
    private final BroadcastReceiver updateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (ACTION_STATE_CHANGED.equals(action)) {
                String identifier = intent.getStringExtra(IDENTIFIER);
                States state = States.getStates(intent.getIntExtra(EXTRA_STATE_CHANGED, 0));

                Log.i(TAG, identifier + " -> " + state.name());

                Toast.makeText(DeviceActivity.this, "State " + state.name() + " for " + identifier, Toast.LENGTH_SHORT).show();

                switch (state) {
                    case NO_CONNECTION:
                        break;
                    case LISTEN:
                        break;
                    case CONNECTING:
                        break;
                    case CONNECTED:
                        break;
                    case ACQUISITION_TRYING:
                        break;
                    case ACQUISITION_OK:
                        break;
                    case ACQUISITION_STOPPING:
                        break;
                    case DISCONNECTED:
                        break;
                    case ENDED:
                        break;

                }
            } else if (ACTION_DATA_AVAILABLE.equals(action)) {
                if (intent.hasExtra(EXTRA_DATA)) {
                    Parcelable parcelable = intent.getParcelableExtra(EXTRA_DATA);
                    if (parcelable.getClass().equals(BITalinoFrame.class)) { //BITalino
                        processResult((BITalinoFrame) parcelable);
                    }
                }
            } else if (ACTION_COMMAND_REPLY.equals(action)) {
                String identifier = intent.getStringExtra(IDENTIFIER);

                if (intent.hasExtra(EXTRA_COMMAND_REPLY) && (intent.getParcelableExtra(EXTRA_COMMAND_REPLY) != null)) {
                    Parcelable parcelable = intent.getParcelableExtra(EXTRA_COMMAND_REPLY);
                    if (parcelable.getClass().equals(BITalinoState.class)) { //BITalino
                        Log.d(TAG, ((BITalinoState) parcelable).toString());
                    } else if (parcelable.getClass().equals(BITalinoDescription.class)) { //BITalino
                    }
                }
            }
        }
    };

    private void processResult(@Nullable BITalinoFrame biTalinoFrame) {
        if (biTalinoFrame == null) return;
//        for (int i = 0; i < bluetoothDevices.size(); i++) {
//            BluetoothDevice bluetoothDevice = bluetoothDevices.get(i);
//            if (biTalinoFrame.getIdentifier().equalsIgnoreCase(bluetoothDevice.getAddress())) {
//                if (i == 0) {
        double ecg = SensorDataConverter.scaleECG(3, biTalinoFrame.getSequence());
        Log.d("VADYM", "Frame " + ecg);
//                } else if (i == 1) {
//
//                }
//            }
//        }
    }

    private IntentFilter makeUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_STATE_CHANGED);
        intentFilter.addAction(ACTION_DATA_AVAILABLE);
        intentFilter.addAction(ACTION_EVENT_AVAILABLE);
        intentFilter.addAction(ACTION_DEVICE_READY);
        intentFilter.addAction(ACTION_COMMAND_REPLY);
        return intentFilter;
    }

    /*
     * Callbacks
     */

    @Override
    public void onBITalinoDataAvailable(BITalinoFrame bitalinoFrame) {
        Message message = handler.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putParcelable(FRAME, bitalinoFrame);
        message.setData(bundle);
        handler.sendMessage(message);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.stop_button:
//                for (BITalinoCommunication biTalinoCommunication : bitalino) {
//                    try {
//                        biTalinoCommunication.stop();
//                    } catch (BITalinoException e) {
//                        e.printStackTrace();
//                    }
//                }
//                break;
        }
    }
}

