package com.bitalino.bitalino;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by VADIM on 04.10.2017.
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}
